@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room" >
<div class="flex-item-3">
    <div class="command-bar flex-container">
        <div class="align-content-left flex-item-1">
            <h2>Event categorie</h2>
        </div>
        <div class="align-content-right flex-item-1">
            <a class="button" href="{{URL::to('EventCategory/Index')}}"> Cancel</a>
            <button class="button" type="submit" form="CreateEventCategoryForm">Create</button>
        </div>
    </div>
    <div class="detail">
        <form method="post" action="{{action('EventCategoryController@create')}}" id="CreateEventCategoryForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="field-container">
                <label for="Name">Naam</label>
                <input type="text" name="Name" id="Name"/>
            </div>
        </form>
    </div>
    @include ('Partial/Errors')
</div>
<div class="flex-item-1">
    <aside class="list">
        @include ('EventCategory/Partial/ReadAll')
    </aside>
@endsection



