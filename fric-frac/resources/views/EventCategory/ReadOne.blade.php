@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="align-content-left flex-item-1">
                <h2>Event categorie</h2>
            </div>
            <div class="align-content-right flex-item-1">
                <a class="button" href="{{URL::to('EventCategory/Index')}}">Cancel</a>
                <a class="button" href="{{URL::to('EventCategory/Updating/'.$eventCategory->Id)}}">Updating</a>
                <button class="button" type="submit" form="DeleteEventCategoryForm">Delete</button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="{{action('EventCategoryController@delete')}}" id="DeleteEventCategoryForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="Id" value="{{$eventCategory->Id}}"/>
            </form>
            <dl>
                <dt>Naam</dt>
                <dd>{{$eventCategory->Name}}</dd>
            </dl>
        </div>
        @include('Partial/Errors')
    </div>
    <div class="flex-item-1">
        <aside class="list">
            @include('EventCategory/Partial/ReadAll')
        </aside>
    </div>
</div>
@endsection
