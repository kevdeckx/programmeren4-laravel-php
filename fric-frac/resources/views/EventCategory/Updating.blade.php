@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>Event categorie</h2>
                </div>
                <div class="align-content-right flex-item-1">
                    <a class="button" href="{{URL::to('EventCategory/Index')}}">Cancel</a>
                     <button class="button" type="submit" form="UpdateEventCategoryForm">Save changes</button>
                </div>
            </div>
            <div class="detail">
            <form method="post" action="{{action('EventCategoryController@update')}}" id="UpdateEventCategoryForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="Id" value="{{$eventCategory->Id}}">
            <div class="field-container">
                <label for="Name">Naam</label>
                <input type="text" name="Name" value="{{$eventCategory->Name}}"/>
            </div>
            </form>
            </div>
            @include('Partial/Errors')
        </div>
        <div class="flex-item-1">
            <aside class="list">
                @include('EventCategory/Partial/ReadAll')
            </aside>
        </div>
    </div>
@endsection