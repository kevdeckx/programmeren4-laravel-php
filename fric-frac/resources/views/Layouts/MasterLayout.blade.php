<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
    </head>
    <body>
        <div id="wrapper">
        <header class="align-content-left">
            <h1>Fric-Frac</h1>
        </header>
        <main>
         @yield('content')
        </main>
        <footer class="align-content-left">
            <a href="{{URL::to('Admin/Index')}}">Beheer</a>
        </footer>
        </div>
    </body>
</html>