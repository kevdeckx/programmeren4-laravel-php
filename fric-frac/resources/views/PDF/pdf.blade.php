<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Event PDF</title>
    
</head>
<body>
    <style type="text/css">
        td{
            border-bottom:1px solid black;
        }
      
        table{
            width:100%;
            margin-bottom:5px;
            padding:5px;
            border:1px solid black;
        }
    </style>
    @for($i = 0; $i < 10; $i++)
    <div style="page-break-inside:avoid;">
    <table>
            <tr>
                <td>Naam</td>
                <td>{{$event->Name}}</td>
            </tr>
            <tr>
                <td>Locatie</td>
                <td>{{$event->Location}}</td>
            </tr>
            <tr>
                <td>Start</td>
                <td>{{$event->Starts}}</td>
            </tr>
            <tr>
                <td>Einde</td>
                <td>{{$event->Ends}}</td>
            </tr>
            <tr>
                <td>Beschrijving</td>
                <td>{{$event->Description}}</td>
            </tr>
            <tr>
                <td>Organisator naam</td>
                <td>{{$event->OrganiserName}}</td>
            </tr>
            <tr>
                <td>Organisator Beschrijving</td>
                <td>{{$event->OrganiserDescription}}</td>
            </tr>
            <tr>
                <td>Categorie</td>
                <td>@if($event->EventCategoryId != null) {{$event->eventCategory->Name}}</td>@endif
            </tr>
            <tr>
                <td>Topic</td>
                <td>@if($event->EventTopicId != null){{$event->EventTopic->Name}}</td>@endif
            </tr>
    </table>
    </div>
    @endfor
</body>
</html>
