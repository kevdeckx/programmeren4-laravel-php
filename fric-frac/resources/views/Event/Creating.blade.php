@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>Event</h2>
                </div>
                <div class="align-content-right flex-item-1">
                     <a class="button" href="{{URL::to('Event/Index')}}">Cancel</a>
                     <button class="button" type="submit" form="CreateEventForm">Create</button>
                </div>
            </div>
            <div class="detail">
            <form method="post" enctype="multipart/form-data" action="{{action('EventController@create')}}" id="CreateEventForm" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="field-container">
            <label for="Name">Naam</label>
            <input type="text" name="Name" id="Name"/>
            </div>
            <div class="field-container">
                <label for="Location">Locatie</label>
                <input type="text" name="Location" id="Location"/>
            </div>
            <div class="field-container">
                <label for="Starts">Start</label>
                <input type="datetime-local" name="Starts" id="Starts"/>
            </div>
             <div class="field-container">
                <label for="Ends">Einde</label>
                <input type="datetime-local" name="Ends" id="Ends"/>
            </div>
            <div class="field-container">
                <label for="Image">Afbeelding</label>
                <input type="file" name="Image" id="Image"/>
            </div>
             <div class="field-container">
                <label for="Description">Beschrijving</label>
               <textarea name="Description" id="Description"></textarea>
            </div>
            <div class="field-container">
                <label for="OrganiserName">Organisator naam</label>
                <input type="text" name="OrganiserName" id="OrganiserName"/>
            </div>
            <div class="field-container">
                <label for="OrganiserDescription">Organisator beschrijving</label>
                <input type="text" name="OrganiserDescription" id="OrganiserDescription"/>
            </div>
            <div class="field-container">
                <label for="EventCategoryId">Event categorie</label>
                <select name="EventCategoryId" id="EventCategoryId">
                  @foreach($eventCategories as $eventCategory)
                  <option value="{{$eventCategory->Id}}">{{$eventCategory->Name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="field-container">
                <label for="EventTopicId">Event topic</label>
                <select name="EventTopicId" id="EventTopicId">
                  @foreach($eventTopics as $eventTopic)
                  <option value="{{$eventTopic->Id}}">{{$eventTopic->Name}}</option>
                  @endforeach
                </select>
            </div>
            </form>
            </div>
        @include('Partial/Errors')
        </div>
    <div class="flex-item-1">
            <aside class="list">
                @include('Event/Partial/ReadAll')
            </aside>
    </div>
    </div>
@endsection



