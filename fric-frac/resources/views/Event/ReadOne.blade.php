@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="align-content-left flex-item-1">
                <h2>Event</h2>
            </div>
            <div class="align-content-right flex-item-1">
                <a class="button" href="{{URL::to('Event/Index')}}">Cancel</a>
                <a class="button" href="{{URL::to('Event/Updating/'.$event->Id)}}">Updating</a>
                <button class="button" type="submit" form="DeleteEventForm">Delete</button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="{{action('EventController@delete')}}" id="DeleteEventForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="Id" value="{{$event->Id}}"/>
            </form>
            <dl>
                <dt>Naam</dt>
                <dd>{{$event->Name}}</dd>
                <dt>Locatie</dt>
                <dd>{{$event->Location}}</dd>
                <dt>Start</dt>
                <dd>{{$event->Starts}}</dd>
                <dt>Einde</dt>
                <dd>{{$event->Ends}}</dd>
                <dt>Afbeelding</dt>
                <dd><img src="{{asset('img/'.$event->Image)}}" width=300></img></dd>
                <dt>Beschrijving</dt>
                <dd>{{$event->Description}}</dd>
                <dt>Organisator naam</dt>
                <dd>{{$event->OrganiserName}}</dd>
                <dt>Organisator beschrijving</dt>
                <dd>{{$event->OrganiserDescription}}</dd>
                <dt>Categorie</dt>
                <dd>
                @if($event->EventCategoryId != null)
                {{$event->EventCategory->Name}}
                </dd>
                @endif
                <dt>Topic</dt>
                <dd>
                @if($event->EventTopicId != null)
                {{$event->EventTopic->Name}}
                </dd>
                @endif
            </dl>
            <a href="{{url::to('downloadPDF/'.$event->Id)}}">Download PDF</a>
        </div>
        
        @include('Partial/Errors')
    </div>
    <div class="flex-item-1">
        <aside class="list">
            @include('Event/Partial/ReadAll')
        </aside>
    </div>
</div>
@endsection
