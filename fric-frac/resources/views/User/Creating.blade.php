@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>User</h2>
                </div>
                <div class="align-content-right flex-item-1">
                     <a class="button" href="{{URL::to('User/Index')}}">Cancel</a>
                     <button class="button" type="submit" form="CreateUserForm">Create</button>
                </div>
            </div>
            <div class="detail">
            <form method="post" action="{{action('UserController@create')}}" id="CreateUserForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="field-container">
            <label for="Name">Naam</label>
            <input type="text" name="Name" id="Name"/>
            </div>
            <div class="field-container">
                <label for="Salt">Zout</label>
                <input type="text" name="Salt" id="Salt"/>
            </div>
            <div class="field-container">
                <label for="HashedPassword">Hash</label>
                <input type="password" name="HashedPassword" id="HashedPassword"/>
            </div>
            <div class="field-container">
                <label for="PersonId">Persoon id</label>
                <select name="PersonId" id="PersonId">
                    @foreach($people as $person)
                    <option value="{{$person->Id}}">{{$person->FirstName.' '.$person->LastName}}</option>
                    @endforeach
                </select>
            </div>
            <div class="field-container">
                <label for="RoleId">Rol id</label>
                <select name="RoleId" id="RoleId">
                @foreach($roles as $role)
                  <option value="{{$role->Id}}">{{$role->Name}}</option>
                @endforeach
                </select>
            </div>
            </form>
            </div>
             @include('Partial/Errors')
        </div>
        <div class="flex-item-1">
            <aside class="list">
                @include('User/Partial/ReadAll')
            </aside>
        </div>
    </div>
@endsection



