@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="align-content-left flex-item-1">
                <h2>User</h2>
            </div>
            <div class="align-content-right flex-item-1">
                <a class="button" href="{{URL::to('User/Index')}}">Cancel</a>
                <a class="button" href="{{URL::to('User/Updating/'.$user->Id)}}">Updating</a>
                <button class="button" type="submit" form="DeleteUserForm">Delete</button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="{{action('UserController@delete')}}" id="DeleteUserForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="Id" value="{{$user->Id}}"/>
            </form>
            <dl>
                <dt>Gebruikersnaam</dt>
                <dd>{{$user->Name}}</dd>
                <dt>Voornaam</dt>
                <dd>
                @if($user->PersonId != null)
                {{$user->person->FirstName}}
                </dd>
                @endif
                <dt>Familienaam</dt>
                <dd>
                @if($user->PersonId != null)
                {{$user->person->LastName}}
                @endif
                </dd>
                <dt>Rol</dt>
                <dd>
                @if($user->RoleId != null) 
                {{$user->role->Name}}
                @endif
                </dd>
            </dl>
        </div>
        @include('Partial/Errors')
    </div>
    <div class="flex-item-1">
        <aside class="list">
            @include('User/Partial/ReadAll')
        </aside>
    </div>
</div>
@endsection
