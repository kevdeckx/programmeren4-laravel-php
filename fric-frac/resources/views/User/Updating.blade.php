@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>User</h2>
                </div>
                <div class="align-content-right flex-item-1">
                    <a class="button" href="{{URL::to('User/Index')}}">Cancel</a>
                     <button class="button" type="submit" form="UpdateUserForm">Save changes</button>
                </div>
            </div>
            <div class="detail">
            <form method="post" action="{{action('UserController@update')}}" id="UpdateUserForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="Id" value="{{$user->Id}}">
             <div class="field-container">
            <label for="Name">Naam</label>
            <input type="text" name="Name" id="Name" value="{{$user->Name}}"/>
            </div>
            <div class="field-container">
                <label for="Salt">Zout</label>
                <input type="text" name="Salt" id="Salt" value="{{$user->Salt}}"/>
            </div>
            <div class="field-container">
                <label for="HashedPassword">Hash</label>
                <input type="password" name="HashedPassword" id="HashedPassword"/>
            </div>
            <div class="field-container">
                <label for="PersonId">Persoon id</label>
                <select name="PersonId" id="PersonId">
                    @foreach($people as $person)
                    @if($user->PersonId == $person->Id)
                    <option value="{{$person->Id}}" selected>{{$person->FirstName.' '.$person->LastName}}</option>
                    @else
                    <option value="{{$person->Id}}">{{$person->FirstName.' '.$person->LastName}}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="field-container">
                <label for="RoleId">Rol id</label>
                <select name="RoleId" id="RoleId">
                @foreach($roles as $role)
                @if($role->Id == $person->RoleId)
                <option value="{{$role->Id}}" selected>{{$role->Name}}</option>
                @else
                <option value="{{$role->Id}}">{{$role->Name}}</option>
                @endif
                @endforeach
                </select>
            </div>
            </form>
            </div>
            @include('Partial/Errors')
        </div>
        <div class="flex-item-1">
            <aside class="list">
                @include('User/Partial/ReadAll')
            </aside>
        </div>
    </div>
@endsection