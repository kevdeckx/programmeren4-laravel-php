@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>Person</h2>
                </div>
                <div class="align-content-right flex-item-1">
                     <a class="button" href="{{URL::to('Person/Index')}}">Cancel</a>
                     <button class="button" type="submit" form="CreatePersonForm">Create</button>
                </div>
            </div>
            <div class="detail">
            <form method="post" action="{{action('PersonController@create')}}" id="CreatePersonForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="field-container">
            <label for="FirstName">Voornaam</label>
            <input type="text" name="FirstName" id="FirstName"/>
            </div>
            <div class="field-container">
                <label for="LastName">Familienaam</label>
                <input type="text" name="LastName" id="LastName"/>
            </div>
            <div class="field-container">
                <label for="Email">E-mail</label>
                <input type="e-mail" name="Email" id="Email"/>
            </div>
            <div class="field-container">
                <label for="Adress1">Adres 1</label>
                <input type="text" name="Adress1" id="Adress1"/>
            </div>
             <div class="field-container">
                <label for="Adress2">Adres 2</label>
                <input type="text" name="Adress2" id="Adress2"/>
            </div>
            <div class="field-container">
                <label for="PostalCode">Postcode</label>
                <input type="text" name="PostalCode" id="PostalCode"/>
            </div>
            <div class="field-container">
                <label for="City">Stad</label>
                <input type="text" name="City" id="City"/>
            </div>
            <div class="field-container">
                <label for="CountryId">Land</label>
                <select name="CountryId" id="CountryId">
                  @foreach($countries as $country)
                  <option value="{{$country->Id}}">{{$country->Name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="field-container">
                <label for="Phone1">Telefoon</label>
                <input type="text" name="Phone1" id="Phone1"/>
            </div>
            <div class="field-container">
                <label for="BirthDay">Geboortedatum</label>
                <input type="date" name="BirthDay" id="BirthDay"/>
            </div>
            </form>
            </div>
        @include('Partial/Errors')
        </div>
    <div class="flex-item-1">
            <aside class="list">
                @include('Person/Partial/ReadAll')
            </aside>
    </div>
    </div>
@endsection



