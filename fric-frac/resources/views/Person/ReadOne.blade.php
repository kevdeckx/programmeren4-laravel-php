@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="align-content-left flex-item-1">
                <h2>Person</h2>
            </div>
            <div class="align-content-right flex-item-1">
                <a class="button" href="{{URL::to('Person/Index')}}">Cancel</a>
                <a class="button" href="{{URL::to('Person/Updating/'.$person->Id)}}">Updating</a>
                <button class="button" type="submit" form="DeletePersonForm">Delete</button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="{{action('PersonController@delete')}}" id="DeletePersonForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="Id" value="{{$person->Id}}"/>
            </form>
            <dl>
                <dt>Voornaam</dt>
                <dd>{{$person->FirstName}}</dd>
                <dt>Familienaam</dt>
                <dd>{{$person->LastName}}</dd>
                <dt>E-mail</dt>
                <dd>{{$person->Email}}</dd>
                <dt>Adres 1</dt>
                <dd>{{$person->Adress1}}</dd>
                <dt>Adres 2</dt>
                <dd>{{$person->Adress2}}</dd>
                <dt>Postcode</dt>
                <dd>{{$person->PostalCode}}</dd>
                <dt>Stad</dt>
                <dd>{{$person->City}}</dd>
                <dt>Land</dt>
                <dd>
                @if($person->CountryId != null)
                {{$person->country->Name}}</dd>
                @endif
                <dt>Telefoon</dt>
                <dd>{{$person->Phone1}}</dd>
                <dt>Geboortedatum</dt>
                <dd>{{$person->BirthDay}}</dd>
            </dl>
        </div>
        @include('Partial/Errors')
    </div>
    <div class="flex-item-1">
        <aside class="list">
            @include('Person/Partial/ReadAll')
        </aside>
    </div>
</div>
@endsection
