@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>Country</h2>
                </div>
                <div class="align-content-right flex-item-1">
                    <a class="button" href="{{URL::to('Country/Index')}}">Cancel</a>
                     <button class="button" type="submit" form="UpdateCountryForm">Save changes</button>
                </div>
            </div>
            <div class="detail">
            <form method="post" action="{{action('CountryController@update')}}" id="UpdateCountryForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="Id" value="{{$country->Id}}">
            <div class="field-container">
                <label for="Name">Land naam</label>
                <input type="text" name="Name" value="{{$country->Name}}"/>
            </div>
            <div class="field-container">
                <label for="Code">Land code</label>
                <input type="text" name="Code" value="{{$country->Code}}"/>
            </div>
            </form>
            </div>
            @include('Partial/Errors')
        </div>
        <div class="flex-item-1">
            <aside class="list">
                @include('Country/Partial/ReadAll')
            </aside>
        </div>
    </div>
@endsection