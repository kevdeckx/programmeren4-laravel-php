@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>Country</h2>
                </div>
                <div class="align-content-right flex-item-1">
                    <a class="button" href="{{ URL::to('Country/Creating') }}">Create new</a>
                </div>
            </div>
            <div class="detail">
            </div>
             @include('Partial/Errors')
        </div>
        <div class="flex-item-1">
            <aside class="list">
                @include('Country/Partial/ReadAll')
            </aside>
        </div>
    </div>
@endsection













