@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="align-content-left flex-item-1">
                <h2>Country</h2>
            </div>
            <div class="align-content-right flex-item-1">
                <a class="button" href="{{URL::to('Country/Index')}}">Cancel</a>
                <a class="button" href="{{URL::to('Country/Updating/'.$country->Id)}}">Updating</a>
                <button class="button" type="submit" form="DeleteCountryForm">Delete</button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="{{action('CountryController@delete')}}" id="DeleteCountryForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="Id" value="{{$country->Id}}"/>
            </form>
            <dl>
                <dt>Land naam</dt>
                <dd>{{$country->Name}}</dd>
                <dt>Land code</dt>
                <dd>{{$country->Code}}</dd>
            </dl>
        </div>
        @include('Partial/Errors')
    </div>
    <div class="flex-item-1">
        <aside class="list">
            @include('Country/Partial/ReadAll')
        </aside>
    </div>
</div>
@endsection
