@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room" >
<div class="flex-item-3">
    <div class="command-bar flex-container">
        <div class="align-content-left flex-item-1">
            <h2>Country</h2>
        </div>
        <div class="align-content-right flex-item-1">
            <a class="button" href="{{URL::to('Country/Index')}}"> Cancel</a>
            <button class="button" type="submit" form="CreateCountryForm">Create</button>
        </div>
    </div>
    <div class="detail">
        <form method="post" action="{{action('CountryController@create')}}" id="CreateCountryForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="field-container">
                <label for="Name"> Land naam </label>
                <input type="text" name="Name"/>
            </div>
            <div class="field-container">
                <label for="Code"> Land code </label>
                <input type="text" name="Code"/>
            </div>
        </form>
    </div>
    @include ('Partial/Errors')
</div>
<div class="flex-item-1">
    <aside class="list">
        @include ('Country/Partial/ReadAll')
    </aside>
@endsection



