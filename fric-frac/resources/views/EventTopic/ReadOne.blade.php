@extends('Layouts.MasterLayout')
@section('content')
<div class="flex-container show-room">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="align-content-left flex-item-1">
                <h2>Event topic</h2>
            </div>
            <div class="align-content-right flex-item-1">
                <a class="button" href="{{URL::to('EventTopic/Index')}}">Cancel</a>
                <a class="button" href="{{URL::to('EventTopic/Updating/'.$eventTopic->Id)}}">Updating</a>
                <button class="button" type="submit" form="DeleteEventTopicForm">Delete</button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="{{action('EventTopicController@delete')}}" id="DeleteEventTopicForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="Id" value="{{$eventTopic->Id}}"/>
            </form>
            <dl>
                <dt>Naam</dt>
                <dd>{{$eventTopic->Name}}</dd>
            </dl>
        </div>
        @include('Partial/Errors')
    </div>
    <div class="flex-item-1">
        <aside class="list">
            @include('EventTopic/Partial/ReadAll')
        </aside>
    </div>
</div>
@endsection
