@extends('Layouts.MasterLayout')
@section('content')
    <div class="flex-container show-room">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <div class="align-content-left flex-item-1">
                    <h2>Role</h2>
                </div>
                <div class="align-content-right flex-item-1">
                    <a class="button" href="{{URL::to('Role/Index')}}">Cancel</a>
                     <button class="button" type="submit" form="UpdateRoleForm">Save changes</button>
                </div>
            </div>
            <div class="detail">
            <form method="post" action="{{action('RoleController@update')}}" id="UpdateRoleForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="Id" value="{{$role->Id}}">
            <div class="field-container">
                <label for="Name">Naam</label>
                <input type="text" name="Name" value="{{$role->Name}}"/>
            </div>
            </form>
            </div>
            @include('Partial/Errors')
        </div>
    <div class="flex-item-1">
            <aside class="list">
                @include('Role/Partial/ReadAll')
            </aside>
    </div>
</div>
@endsection