@extends('Layouts.MasterLayout')
@section('content')
        <div class="show-room">
            <div class="flex-container">
                <div class="flex-item-1 tile align-content-center">
                    <a href="../Person/Index">Person</a>
                </div>
                <div class="flex-item-1 tile align-content-center">
                     <a href="../Country/Index">Country</a>
                </div>
                <div class="flex-item-2 tile align-content-center"></div>
            </div>
            <div class="flex-container">
                <div class="flex-item-1 tile"></div>
                <div class="flex-item-1 tile align-content-center"> 
                    <a href="../Role/Index">Role</a>
                </div>
                <div class="flex-item-1 tile align-content-center">
                    <a href="../User/Index">User</a>
                </div>
                <div class="flex-item-1 tile"></div>
            </div>
                <div class="flex-container">
                <div class="flex-item-1 tile align-content-center">
                    <a href="../Event/Index">Event</a>
                </div>
                <div class="flex-item-1 tile align-content-center">
                    <a href="../EventCategory/Index">Event category</a>
                </div>
                <div class="flex-item-1 tile align-content-center">
                         <a href="../EventTopic/Index">Event topic</a>
                </div>
                <div class="flex-item-1 tile"></div>
            </div>
        </div>
@endsection
