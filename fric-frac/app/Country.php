<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
      protected $fillable = [
        'Name',
        'Code'
        ];
    protected $table = 'Country';
    protected $primaryKey = 'Id';
    public $timestamps = false;
    
}
