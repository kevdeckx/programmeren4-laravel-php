<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'Name',
        'Location',
        'Starts',
        'Ends',
        'Image',
        'Description',
        'OrganiserName',
        'OrganiserDescription',
        'EventCategoryId',
        'EventTopicId'
        ];
        
    protected $table = 'Event';
    protected $primaryKey = 'Id';
    public $timestamps = false;
    
    public function eventCategory()
    {
        return $this->belongsTo('App\EventCategory', 'EventCategoryId');
    }
    
    public function eventTopic()
    {
        return $this->belongsTo('App\EventTopic', 'EventTopicId');
    }
}
