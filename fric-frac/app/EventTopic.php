<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTopic extends Model
{
    protected $table = 'EventTopic';
    protected $primaryKey = 'Id';
    public $timestamps = false;

    protected $fillable = [
        'Name'
        ];
   
}
