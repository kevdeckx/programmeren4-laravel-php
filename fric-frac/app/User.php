<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'User';
    protected $primaryKey = 'Id';
    public $timestamps = false;
    
    protected $fillable = [
        'Name',
        'Salt',
        'HashedPassword',
        'PersonId',
        'RoleId'
        ];
        
    public function person()
    {
        return $this->belongsTo('App\Person', 'PersonId');
    }
    
    public function role()
    {
        return $this->belongsTo('App\Role', 'RoleId');
    }
}
