<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{ 
    protected $fillable = ['FirstName',
                           'LastName',
                           'Email',
                           'Password',
                           'Adress1',
                           'Adress2',
                           'PostalCode',
                           'City',
                           'CountryId',
                           'Phone1',
                           'BirthDay',
                           'Rating'
                           ];
    protected $table = 'Person';
    protected $primaryKey = 'Id';
    public $timestamps = false;
    
    public function country()
    {
        return $this->belongsTo('App\Country', 'CountryId');
    }
   
}
