<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventTopic;

class EventTopicController extends Controller
{
    public function index(){
        $eventTopics = EventTopic::all();
        return view('EventTopic/Index', array('eventTopics' => $eventTopics));
    }
    
    public function creating(){
        $eventTopics = EventTopic::all();
        return view('EventTopic/Creating', array('eventTopics' => $eventTopics));
    }
    
    public function create(Request $request){
        $this->validate($request,['Name' => 'required|unique:EventTopic,Name|max:120']);
        $eventTopic = new EventTopic($request->all());
        $eventTopic->save();
        return redirect()->action('EventTopicController@index');
    }
    
    public function readOne($id){
        $eventTopic = EventTopic::find($id);
        $eventTopics = EventTopic::all();
        return view('EventTopic/ReadOne', array('eventTopic' => $eventTopic, 'eventTopics' => $eventTopics));
    }
    
    public function updating($id){
        $eventTopic = EventTopic::find($id);
        $eventTopics = EventTopic::all();
        return view('EventTopic/Updating', array('eventTopic' => $eventTopic, 'eventTopics' => $eventTopics));
    }
    
    public function update(Request $request){
        $this->validate($request,['Name' => 'required|unique:EventTopic,Name|max:120']);
        $eventTopic = EventTopic::find($request->input('Id'));
        $eventTopic->update($request->all());
        return redirect()->action('EventTopicController@index');
    }
    
   public function delete(Request $request){
       $eventTopic = EventTopic::find($request->input('Id'));
       $eventTopic->delete();
       return redirect()->action('EventTopicController@index');
   }
}
