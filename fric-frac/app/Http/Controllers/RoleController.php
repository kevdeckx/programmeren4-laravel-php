<?php

namespace App\Http\Controllers;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
     public function index(){
        $roles = Role::all();
        return view('Role/Index', array('roles' => $roles));
    }
    
    public function creating(){
        $roles = Role::all();
        return view('Role/Creating', array('roles' => $roles));
    }
    
    public function create(Request $request){
        $this->validate($request, ['Name' => 'required|unique:Role,Name|max:50']);
        $role = new Role($request->all());
        $role->save();
        return redirect()->action('RoleController@index');
    }
    
    public function readOne($id){
        $role = Role::find($id);
        $roles = Role::all();
        return view('Role/ReadOne', array('role' => $role, 'roles' => $roles));
    }
    
    public function updating($id){
        $role = Role::find($id);
        $roles = Role::all();
        return view('Role/Updating', array('role' => $role, 'roles' => $roles));
    }
    
    public function update(Request $request){
        $this->validate($request, ['Name' => 'required|unique:Role,Name|max:50']);
        $role = Role::find($request->input('Id'));
        $role->update($request->all());
        return redirect()->action('RoleController@index');
    }
    
   public function delete(Request $request){
       $role = Role::find($request->input('Id'));
       $role->delete();
       return redirect()->action('RoleController@index');
   }
}
