<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Person;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        return view('User.Index', array('users' => $users));
    }
    
    public function creating(){
       $people = Person::all();
       $roles = Role::all();
       $users = User::all();
       return view('User.Creating', array('users' => $users, 'people' => $people, 'roles' => $roles));
    }
    
    public function create(Request $request){
        $this->validate($request, ['Name' => 'required|unique:User,Name|max:50',
                                   'Salt' => 'nullable|max:255',
                                   'HashedPassword' => 'required|max:255',
                                   'PersonId' => 'integer',
                                   'RoleId' => 'integer']);
        $user = new User($request->all());
        $user->save();
        return redirect()->action('UserController@index');
   }
    
    public function readOne($id){
        $user = User::find($id);
        $users = User::all();
        return view('User.ReadOne', array('user' => $user, 'users' => $users));
    }
    
    public function updating($id){
        $user = User::find($id);
        $users = User::all();
        $people = Person::all();
        $roles = Role::all();
        return view('User.Updating', array('user' => $user, 'users' => $users, 'people' => $people, 'roles' => $roles));
    }
    
    public function update(Request $request){
        $this->validate($request, ['Name' => 'required|unique:User,Name|max:50',
                                   'Salt' => 'nullable|max:255',
                                   'HashedPassword' => 'required|max:255',
                                   'PersonId' => 'integer',
                                   'RoleId' => 'integer']);
                                   
        $user = User::find($request->input('Id'));
        $user->update($request->all());
        return redirect()->action('UserController@index');
    }
    
    public function delete(Request $request){
       $user = User::find($request->input('Id'));
       $user->delete();
       return redirect()->action('UserController@index');
   }
          
}
