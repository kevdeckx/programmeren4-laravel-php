<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Country;


class PersonController extends Controller
{
    
    public function index(){
        $people = Person::all();
        return view('Person.Index', array('people' => $people));
    }
    
    public function creating(){
       $countries = Country::all();
       $people = Person::all();
       return view('Person.Creating', array('people' => $people, 'countries' => $countries));
    }
    
    public function create(Request $request){
    $this->validate($request, ['FirstName' => 'required|max:50',
                               'LastName' => 'required|max:120',
                               'Email' => 'nullable|email|max:255',
                               'Adress1' => 'nullable|max:255',
                               'Adress2' => 'nullable|max:255',
                               'PostalCode' => 'nullable|max:20',
                               'City' => 'nullable|max:80',
                               'CountryId' => 'nullable|integer',
                               'Phone1' => 'nullable|max:25',
                               'BirthDay' => 'nullable|date',
                               'Rating' => 'nullable|integer'
                               ]);
    $person = new Person($request->all());
    $person->save();
    return redirect()->action('PersonController@index');
   }
    
    public function readOne($id){
        $person = Person::find($id);
        $people = Person::all();
        return view('Person.ReadOne', array('person' => $person, 'people' => $people));
    }
    
    public function updating($id){
        $person = Person::find($id);
        $formattedDate = date('Y-m-d', strtotime($person->BirthDay));
        $person->BirthDay = $formattedDate;
        $people = Person::all();
        $countries = Country::all();
        return view('Person.Updating', array('person' => $person, 'people' => $people, 'countries' => $countries));
    }
    
    public function update(Request $request){
     $this->validate($request, ['FirstName' => 'required|max:50',
                               'LastName' => 'required|max:120',
                               'Email' => 'nullable|email|max:255',
                               'Adress1' => 'nullable|max:255',
                               'Adress2' => 'nullable|max:255',
                               'PostalCode' => 'nullable|max:20',
                               'City' => 'nullable|max:80',
                               'CountryId' => 'nullable|integer',
                               'Phone1' => 'nullable|max:25',
                               'BirthDay' => 'nullable|date',
                               'Rating' => 'nullable|integer'
                               ]);
        $person = Person::find($request->input('Id'));
        $person->update($request->all());
        return redirect()->action('PersonController@index');
    }
    
   public function delete(Request $request){
       $person = Person::find($request->input('Id'));
       $person->delete();
       return redirect()->action('PersonController@index');
   }
         
}
