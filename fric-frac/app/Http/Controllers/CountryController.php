<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    public function index(){
        $countries = Country::all();
        return view('Country/Index', array('countries' => $countries));
    }
    
    public function creating(){
        $countries = Country::all();
        return view('Country/Creating', array('countries' => $countries));
    }
    
    public function create(Request $request){
        $this->validate($request,['Name' => 'required|unique:Country,Name|max:50',
                                  'Code' => 'unique:Country,Code|max:2']);
        
        $country = new Country($request->all());
        $country->save();
        return redirect()->action('CountryController@index');
    }
    
    public function readOne($id){
        $country = Country::find($id);
        $countries = Country::all();
        return view('Country/ReadOne', array('country' => $country, 'countries' => $countries));
    }
    
    public function updating($id){
        $country = Country::find($id);
        $countries = Country::all();
        return view('Country/Updating', array('country' => $country, 'countries' => $countries));
    }
    
    public function update(Request $request){
        $this->validate($request,['Name' => 'required|unique:Country,Name|max:50',
                                  'Code' => 'nullable|unique:Country,Code|max:2']);
                                  
        $country = Country::find($request->input('Id'));
        $country->update($request->all());
        return redirect()->action('CountryController@index');
    }
    
   public function delete(Request $request){
       $country = Country::find($request->input('Id'));
       $country->delete();
       return redirect()->action('CountryController@index');
   }
}

?>