<?php

namespace App\Http\Controllers;
use App\Event;
use App\EventCategory;
use App\EventTopic;
use PDF;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index(){
        $events = Event::all();
        return view('Event/Index', array('events' => $events));
    }
    
    public function creating(){
        $eventCategories = EventCategory::all();
        $eventTopics = EventTopic::all();
        $events = Event::all();
        return view('Event/Creating', array('events' => $events, 'eventCategories' => $eventCategories, 'eventTopics' => $eventTopics));
    }
    
    public function create(Request $request){
        $this->validate($request,['Name' => 'required|max:120',
                                  'Location' => 'required|max:120',
                                  'Starts' => 'nullable|date',
                                  'Ends' => 'nullable|date',
                                  'Image' => 'required|image',
                                  'Description' => 'required|max:1024',
                                  'OrganiserName' => 'required|max:120',
                                  'OrganiserDescription' => 'required|max:120',
                                  'EventCategoryId' => 'nullable|integer',
                                  'EventTopicId' => 'nullable|integer'
                                  ]);
        $event = new Event($request->all());
        
        $image = $request->file('Image');
        $name = time().'.'.$image->getClientOriginalExtension();
        $event->Image = $name;
        $image->move(public_path('/img'), $name);
        $event->save();
        return redirect()->action('EventController@index');
    }
    
    public function readOne($id){
        $event = Event::find($id);
        $events = Event::all();
        return view('Event/ReadOne', array('events' => $events, 'event' => $event));
    }
    
    public function updating($id){
        $eventCategories = EventCategory::all();
        $eventTopics = EventTopic::all();
        $event = Event::find($id);
        $formattedStartDate = date('Y-m-d\TH:i', strtotime($event->Starts));
        $formattedEndDate = date('Y-m-d\TH:i', strtotime($event->Ends));
        $event->Starts = $formattedStartDate;
        $event->Ends = $formattedEndDate;
        $events = Event::all();
        return view('Event/Updating', array('events' => $events, 'event' => $event, 'eventCategories' =>$eventCategories, 'eventTopics' => $eventTopics));
    }
 
    public function update(Request $request){
        $event = Event::find($request->input('Id'));
        $requestData = $request->all();
        //check if file was selected
        if ($request->hasFile('Image')) {
        //if set, validate and check for image
        $this->validate($request, ['Image' => 'required|image']);
        $file = public_path('img/'.$event->Image);
        //if file exists, delete and build new url to store in database
        if(file_exists($file)){
        unlink($file);
        $image = $request->file('Image');
        $name = time().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('/img'), $name);
        //manipulate $request parameter as we will store a path and not the image
        $requestData['Image'] = $name;
        }
        }else{
            $requestData['Image'] = $event->Image;
        }
        $this->validate($request,['Name' => 'required|max:120',
                                  'Location' => 'required|max:120',
                                  'Starts' => 'nullable|date',
                                  'Ends' => 'nullable|date',
                                  'Description' => 'required|max:1024',
                                  'OrganiserName' => 'required|max:120',
                                  'OrganiserDescription' => 'required|max:120',
                                  'EventCategoryId' => 'nullable|integer',
                                  'EventTopicId' => 'nullable|integer'
                                  ]);
        
        $event->update($requestData);
        return redirect()->action('EventController@index');
    }
    
    public function delete(Request $request){
       $event = Event::find($request->input('Id'));
       $file = public_path('img/'.$event->Image);
       if(file_exists($file)){
           unlink($file);
       }
       $event->delete();
       return redirect()->action('EventController@index');
   }
   
     public function downloadPDF($id){
      $event = Event::find($id);
      $pdf = PDF::loadView('PDF/pdf', compact('event'));
      return $pdf->download('event.pdf');
      
    }
}
