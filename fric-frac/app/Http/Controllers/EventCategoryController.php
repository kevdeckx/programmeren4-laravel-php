<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventCategory;

class EventCategoryController extends Controller
{
  public function index(){
        $eventCategories = EventCategory::all();
        return view('EventCategory/Index', array('eventCategories' => $eventCategories));
    }
    
    public function creating(){
        $eventCategories = EventCategory::all();
        return view('EventCategory/Creating', array('eventCategories' => $eventCategories));
    }
    
    public function create(Request $request){
        $this->validate($request,['Name' => 'required|unique:EventCategory,Name|max:120']);
        $eventCategory = new EventCategory($request->all());
        $eventCategory->save();
        return redirect()->action('EventCategoryController@index');
    }
    
    public function readOne($id){
        $eventCategory = EventCategory::find($id);
        $eventCategories = EventCategory::all();
        return view('EventCategory/ReadOne', array('eventCategory' => $eventCategory, 'eventCategories' => $eventCategories));
    }
    
    public function updating($id){
        $eventCategory = EventCategory::find($id);
        $eventCategories = EventCategory::all();
        return view('EventCategory/Updating', array('eventCategory' => $eventCategory, 'eventCategories' => $eventCategories));
    }
    
    public function update(Request $request){
        $this->validate($request,['Name' => 'required|unique:EventCategory,Name|max:120']);
        $eventCategory = EventCategory::find($request->input('Id'));
        $eventCategory->update($request->all());
        return redirect()->action('EventCategoryController@index');
    }
    
   public function delete(Request $request){
       $eventCategory = EventCategory::find($request->input('Id'));
       $eventCategory->delete();
       return redirect()->action('EventCategoryController@index');
   }
}
