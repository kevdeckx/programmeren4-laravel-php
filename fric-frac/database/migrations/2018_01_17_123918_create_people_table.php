<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Person', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('FirstName', 50);
            $table->string('LastName', 120);
            $table->string('Email', 255)->nullable();
            $table->string('Adress1', 255)->nullable();
            $table->string('Adress2', 255)->nullable();
            $table->string('PostalCode', 20)->nullable();
            $table->string('City', 80)->nullable();
            $table->integer('CountryId')->unsigned()->nullable();
            $table->foreign('CountryId')->references('Id')->on('Country')->onDelete('set null');
            $table->string('Phone1', 25)->nullable();
            $table->datetime('BirthDay')->nullable();
            $table->integer('Rating')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Person');
    }
}
