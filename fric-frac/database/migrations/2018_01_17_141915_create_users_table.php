<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('User', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name', 50);
            $table->string('Salt', 255)->nullable();
            $table->string('HashedPassword', 255);
            $table->integer('PersonId')->unsigned()->nullable();
            $table->foreign('PersonId')->references('Id')->on('Person')->onDelete('cascade');
            $table->integer('RoleId')->unsigned()->nullable();
            $table->foreign('RoleId')->references('Id')->on('Role')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('User');
    }
}
