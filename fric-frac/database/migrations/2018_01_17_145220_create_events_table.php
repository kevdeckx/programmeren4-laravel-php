<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Event', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name', 120);
            $table->string('Location', 120);
            $table->datetime('Starts')->nullable();
            $table->datetime('Ends')->nullable();
            $table->string('Image', 255);
            $table->string('Description', 1024);
            $table->string('OrganiserName', 120);
            $table->string('OrganiserDescription', 120);
            $table->integer('EventCategoryId')->unsigned()->nullable();
            $table->foreign('EventCategoryId')->references('Id')->on('EventCategory')->onDelete('set null');
            $table->integer('EventTopicId')->unsigned()->nullable();
            $table->foreign('EventTopicId')->references('Id')->on('EventTopic')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Event');
    }
}
