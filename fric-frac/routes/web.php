<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/Admin/Index', function () {
    return view('Admin/Index');
});


Route::get('/Person/Index', 'PersonController@index');
Route::get('/Person/ReadOne/{Id}', 'PersonController@readOne');
Route::get('/Person/Creating', 'PersonController@creating');
Route::post('/Person/Create', 'PersonController@create');
Route::get('/Person/Updating/{Id}', 'PersonController@updating');
Route::post('/Person/Update', 'PersonController@update');
Route::post('/Person/Delete', 'PersonController@delete');

Route::get('/Country/Index', 'CountryController@index');
Route::get('/Country/Creating', 'CountryController@creating');
Route::post('/Country/Create', 'CountryController@create');
Route::get('/Country/ReadOne/{Id}', 'CountryController@readOne');
Route::get('/Country/Updating/{Id}', 'CountryController@updating');
Route::post('/Country/Update', 'CountryController@update');
Route::post('/Country/Delete', 'CountryController@delete');

Route::get('/Role/Index', 'RoleController@index');
Route::get('/Role/Creating', 'RoleController@creating');
Route::post('/Role/Create', 'RoleController@create');
Route::get('/Role/ReadOne/{Id}', 'RoleController@readOne');
Route::get('/Role/Updating/{Id}', 'RoleController@updating');
Route::post('/Role/Update', 'RoleController@update');
Route::post('/Role/Delete', 'RoleController@delete');

Route::get('/User/Index', 'UserController@index');
Route::get('/User/Creating', 'UserController@creating');
Route::post('/User/Create', 'UserController@create');
Route::get('/User/ReadOne/{Id}', 'UserController@readOne');
Route::get('/User/Updating/{Id}', 'UserController@updating');
Route::post('/User/Update', 'UserController@update');
Route::post('/User/Delete', 'UserController@delete');

Route::get('/EventCategory/Index', 'EventCategoryController@index');
Route::get('/EventCategory/Creating', 'EventCategoryController@creating');
Route::post('/EventCategory/Create', 'EventCategoryController@create');
Route::get('/EventCategory/ReadOne/{Id}', 'EventCategoryController@readOne');
Route::get('/EventCategory/Updating/{Id}', 'EventCategoryController@updating');
Route::post('/EventCategory/Update', 'EventCategoryController@update');
Route::post('/EventCategory/Delete', 'EventCategoryController@delete');

Route::get('/EventTopic/Index', 'EventTopicController@index');
Route::get('/EventTopic/Creating', 'EventTopicController@creating');
Route::post('/EventTopic/Create', 'EventTopicController@create');
Route::get('/EventTopic/ReadOne/{Id}', 'EventTopicController@readOne');
Route::get('/EventTopic/Updating/{Id}', 'EventTopicController@updating');
Route::post('/EventTopic/Update', 'EventTopicController@update');
Route::post('/EventTopic/Delete', 'EventTopicController@delete');

Route::get('/Event/Index', 'EventController@index');
Route::get('/Event/Creating', 'EventController@creating');
Route::post('/Event/Create', 'EventController@create');
Route::get('/Event/ReadOne/{Id}', 'EventController@readOne');
Route::get('/Event/Updating/{Id}', 'EventController@updating');
Route::post('/Event/Update', 'EventController@update');
Route::post('/Event/Delete', 'EventController@delete');

Route::get('/downloadPDF/{id}','EventController@downloadPDF');
?>